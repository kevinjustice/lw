/*
Kevin R. Justice
033017
themiser.js

the miser interactive album scene(s) for loudness war.
*/

authorizeBrowser = function(){
	var browserPass = (is.chrome() || is.firefox() || is.safari()) && !is.mobile();
	var initializationErrors = global.initializationErrors;

	if(browserPass && !initializationErrors){
		menuHelper.aboutScreen.show();
	}
	else{
		menuHelper.incompatibleScreen.show();
	};
};

strobe = function(t){
	tn = t%(2*Math.PI);
	if(tn>=0 && tn<Math.PI){
		return(1);
	}
	else{
		return(-1);
	};
};

loadFilesDestroy = function(){
	menuHelper.loadingMenu.hide();
	delete global.loadingStartTime;
	console.info('Ready for Next Scene.');
};

loadFilesAnimate = function(){
	/*
	PASS
	*/
};

loadFilesInit = function(){
	menuHelper.aboutScreen.hide();
	animateLoop = loadFilesAnimate;
	destroyFunction = loadFilesDestroy;

	console.info('Loading Models...');

	//Cover the Canvas
	menuHelper.cover.show();

	/*
	Load Objects

	#OBJECT LOADING FORMS#

	-mtl and obj:

	mtlLoader.load('engine/models/obj-mtl/pathto.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/pathto.obj', function(object){
			//special attributes
			global.models.type.name = object;
			menuHelper.loadingMenu.updateProgress(numModels);
		});
	});

	-obj only:
	
	objLoader.load('engine/models/obj-mtl/pathto.obj', function(object){
		var material = new THREE.MeshLambertMaterial({color:'hex'});
		for(var i=0; i<object.children.length; i++){
			object.children[i].material = material;
		};
		//special attributes
		global.models.type.name = object;
		menuHelper.loadingMenu.updateProgress(numModels);
	});
	*/
	var numModels = 38;
	var videoPath = '/themiserinteractive/';

	var mtlLoader = new THREE.MTLLoader();
	var objLoader = new THREE.OBJLoader();
	var txLoader = new THREE.TextureLoader();
	var aLoader = new THREE.AudioLoader();
	var fLoader = new THREE.FontLoader();
	mtlLoader.setPath('/themiserinteractive/');
	objLoader.setPath('/themiserinteractive/');
	txLoader.setPath('/themiserinteractive/');
	aLoader.setPath('/themiserinteractive/');
	fLoader.setPath('/themiserinteractive/');

	var billboardVideoRequest = new XMLHttpRequest();
	billboardVideoRequest.open('GET', videoPath+'engine/video/warfair.mp4', true);
	billboardVideoRequest.responseType = 'blob';
	billboardVideoRequest.onload = function(){
		if (this.status === 200) {
	     	var videoBlob = this.response;
	     	var url = window.URL.createObjectURL(videoBlob);
	     	global.video.billboardVideoURL = url;
		};
	};
	billboardVideoRequest.send();

	var terreVideoRequest = new XMLHttpRequest();
	terreVideoRequest.open('GET', videoPath+'engine/video/coldBowl.mp4', true);
	terreVideoRequest.responseType = 'blob';
	terreVideoRequest.onload = function(){
		if (this.status === 200) {
	     	var videoBlob = this.response;
	     	var url = window.URL.createObjectURL(videoBlob);
	     	global.video.terreVideoURL = url;
		};
	};
	terreVideoRequest.send();

	global.loadingStartTime = performance.now()*.001;
	aLoader.load('engine/audio/GrooveSynth.mp3', function(audioBuffer){
		global.audio.music.grooveSynth = audioBuffer;
		menuHelper.loadingMenu.show();
		menuHelper.cover.hide();

	aLoader.load('engine/audio/album/01Groovefuzz.mp3', function(audioBuffer){
		global.audio.music.grooveFuzz = audioBuffer;

	aLoader.load('engine/audio/album/02Drano.mp3', function(audioBuffer){
		global.audio.music.drano = audioBuffer;

	aLoader.load('engine/audio/album/03The_Miser.mp3', function(audioBuffer){
		global.audio.music.theMiser = audioBuffer;

	aLoader.load('engine/audio/album/04I_Wanna_Disappear.mp3', function(audioBuffer){
		global.audio.music.iWannaDisappear = audioBuffer;

	aLoader.load('engine/audio/album/05Lovely_Trip.mp3', function(audioBuffer){
		global.audio.music.lovelyTrip = audioBuffer;

	aLoader.load('engine/audio/album/06Acid_Face.mp3', function(audioBuffer){
		global.audio.music.acidFace = audioBuffer;

	aLoader.load('engine/audio/album/07Light_Race_Space_Death.mp3', function(audioBuffer){
		global.audio.music.lightRaceSpaceDeath = audioBuffer;

	aLoader.load('engine/audio/album/08Eviscerator.mp3', function(audioBuffer){
		global.audio.music.eviscerator = audioBuffer;

	aLoader.load('engine/audio/album/09Minds_Eye.mp3', function(audioBuffer){
		global.audio.music.mindsEye = audioBuffer;

	aLoader.load('engine/audio/album/10Rut.mp3', function(audioBuffer){
		global.audio.music.rut = audioBuffer;

	aLoader.load('engine/audio/sfx/miserUnlock.mp3', function(audioBuffer){
		global.audio.sfx.miserUnlock = audioBuffer;

	aLoader.load('engine/audio/sfx/trackUnlock.mp3', function(audioBuffer){
		global.audio.sfx.trackUnlock = audioBuffer;

	aLoader.load('engine/audio/sfx/matthewTutorialNarration.mp3', function(audioBuffer){
		global.audio.sfx.matthewTutorialNarration = audioBuffer;

	aLoader.load('engine/audio/sfx/stevieTutorialNarration.mp3', function(audioBuffer){
		global.audio.sfx.stevieTutorialNarration = audioBuffer;

	aLoader.load('engine/audio/sfx/quinnTutorialNarration.mp3', function(audioBuffer){
		global.audio.sfx.quinnTutorialNarration = audioBuffer;

	aLoader.load('engine/audio/sfx/ryanTutorialNarration.mp3', function(audioBuffer){
		global.audio.sfx.ryanTutorialNarration = audioBuffer;

	aLoader.load('engine/audio/sfx/miserTutorialNarration.mp3', function(audioBuffer){
		global.audio.sfx.miserTutorialNarration = audioBuffer;

	fLoader.load('engine/fonts/droid_sans_bold.typeface.json', function(font){
		global.fonts.droidSansBold = font;

	mtlLoader.load('engine/models/obj-mtl/structures/basement.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/structures/basement.obj', function(object){
			//special attributes
			global.models.structures.basement = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	objLoader.load('engine/models/obj-mtl/structures/bathroom.obj', function(object){
		var material = new THREE.MeshLambertMaterial({color:0xffffff});
		for(var i=0; i<object.children.length; i++){
			object.children[i].material = material;
		};
		object.children[1].material = new THREE.MeshLambertMaterial({color:0x939393});
		object.children[2].material = new THREE.MeshLambertMaterial({color:0x939393});
		object.children[3].material = new THREE.MeshLambertMaterial({color:0x939393});
		object.children[5].material = new THREE.MeshLambertMaterial({color:0x939393});
		object.children[6].material = new THREE.MeshLambertMaterial({color:0xfff9ad});
		object.children[7].material = new THREE.MeshLambertMaterial({color:0xd8d8d8});
		global.models.structures.bathroom = object;
		menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/structures/pyramid.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/structures/pyramid.obj', function(object){
			//special attributes
			global.models.structures.pyramid = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	txLoader.load('engine/models/obj-mtl/structures/textures/PANO2.jpg', function(texture){
		var g1 = new THREE.SphereGeometry(500, 60, 40);
		g1.scale(-1,1,1);
		var m1 = new THREE.MeshBasicMaterial({map: texture});
		var me1 = new THREE.Mesh(g1, m1);
		//me1.rotation.set(-.07,0,.11);
		global.models.structures.roomSphere= me1;
		menuHelper.loadingMenu.updateProgress(numModels);
		
	mtlLoader.load('engine/models/obj-mtl/props/guitarAmp.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/props/guitarAmp.obj', function(object){
			//special attributes
			global.models.props.guitarAmp = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/props/guitarCream.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/props/guitar.obj', function(object){
			//special attributes
			global.models.props.guitarCream = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/props/guitarBlack.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/props/guitar.obj', function(object){
			//special attributes
			global.models.props.guitarBlack = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/props/drums.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load('engine/models/obj-mtl/props/drums.obj', function(object){
			object.children[20].visible = false;
			object.children[21].visible = false;
			global.models.props.drums = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/props/album.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/props/album.obj', function(object){
			//special attributes
			global.models.props.album = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/props/album2.mtl', function(materials){
		materials.preload();
		var material = materials.materials['09___Default'];
		var objectNew = object.clone();
		objectNew.children[7].material = material;
		global.models.props.album2 = objectNew;

	mtlLoader.load('engine/models/obj-mtl/props/album3.mtl', function(materials){
		materials.preload();
		var material = materials.materials['09___Default'];
		var objectNew = object.clone();
		objectNew.children[7].material = material;
		global.models.props.album3 = objectNew;

	mtlLoader.load('engine/models/obj-mtl/props/album4.mtl', function(materials){
		materials.preload();
		var material = materials.materials['09___Default'];
		var objectNew = object.clone();
		objectNew.children[7].material = material;
		global.models.props.album4 = objectNew;

	mtlLoader.load('engine/models/obj-mtl/props/album5.mtl', function(materials){
		materials.preload();
		var material = materials.materials['09___Default'];
		var objectNew = object.clone();
		objectNew.children[7].material = material;
		global.models.props.album5 = objectNew;

	mtlLoader.load('engine/models/obj-mtl/props/album6.mtl', function(materials){
		materials.preload();
		var material = materials.materials['09___Default'];
		var objectNew = object.clone();
		objectNew.children[7].material = material;
		global.models.props.album6 = objectNew;

	mtlLoader.load('engine/models/obj-mtl/props/album7.mtl', function(materials){
		materials.preload();
		var material = materials.materials['09___Default'];
		var objectNew = object.clone();
		objectNew.children[7].material = material;
		objectNew.children[3].visible = false;
		global.models.props.album7 = objectNew;

	mtlLoader.load('engine/models/obj-mtl/props/album8.mtl', function(materials){
		materials.preload();
		var material = materials.materials['09___Default'];
		var objectNew = object.clone();
		objectNew.children[7].material = material;
		objectNew.children[3].visible = false;
		global.models.props.album8 = objectNew;

	mtlLoader.load('engine/models/obj-mtl/props/bass.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/props/bass.obj', function(object){
			//special attributes
			global.models.props.bass = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/props/bassCab.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/props/bassCab.obj', function(object){
			//special attributes
			global.models.props.bassCab = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/props/bassHead.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/props/bassHead.obj', function(object){
			//special attributes
			global.models.props.bassHead = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	objLoader.load('engine/models/obj-mtl/props/beanBag.obj', function(object){
		var material = new THREE.MeshLambertMaterial({color:0xff0000});
		for(var i=0; i<object.children.length; i++){
			object.children[i].material = material;
		};
		global.models.props.beanBagRed = object;
		var objectNew = object.clone();
		var material = new THREE.MeshLambertMaterial({color:0xff3dff});
		for(var i=0; i<objectNew.children.length; i++){
			objectNew.children[i].material = material;
		};
		global.models.props.beanBagPurple = objectNew;
		var objectNew = object.clone();
		var material = new THREE.MeshLambertMaterial({color:0xffff00});
		for(var i=0; i<objectNew.children.length; i++){
			objectNew.children[i].material = material;
		};
		global.models.props.beanBagYellow = objectNew;
		menuHelper.loadingMenu.updateProgress(numModels);

	objLoader.load('engine/models/obj-mtl/props/lavaLamp.obj', function(object){
		var m1 = new THREE.MeshLambertMaterial({color:0x727272});
		var m2 = new THREE.MeshLambertMaterial({color:0x0055ff, transparent: true, opacity: 0.5});
		var m3 = new THREE.MeshLambertMaterial({color:0x21ff00, transparent: true});
		for(var i=0; i<object.children.length; i++){
			object.children[i].material = m1;
		};
		object.children[4].material = m2;
		object.children[2].material = m3;
		object.children[1].material = m3;
		object.children[0].material = m3;
		global.models.props.lavaLampLargeBlue = object;
		var objectNew = object.clone();
		var m1 = new THREE.MeshLambertMaterial({color:0x727272});
		var m2 = new THREE.MeshLambertMaterial({color:0x000000, transparent: true, opacity: 0.5});
		var m3 = new THREE.MeshLambertMaterial({color:0xff0000, transparent: true});
		for(var i=0; i<objectNew.children.length; i++){
			objectNew.children[i].material = m1;
		};
		objectNew.children[4].material = m2;
		objectNew.children[2].material = m3;
		objectNew.children[1].material = m3;
		objectNew.children[0].material = m3;
		global.models.props.lavaLampSmallRed = objectNew;
		menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/props/stereoSpeaker.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/props/stereoSpeaker.obj', function(object){
			//special attributes
			global.models.props.stereoSpeaker = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	objLoader.load('engine/models/obj-mtl/props/floorLamp.obj', function(object){
		var material = new THREE.MeshLambertMaterial({color:0xffffff});
		for(var i=0; i<object.children.length; i++){
			object.children[i].material = material;
		};
		//special attributes
		global.models.props.floorLamp = object;
		menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/props/rug.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/props/rug.obj', function(object){
			//special attributes
			global.models.props.rug = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/props/rug2.mtl', function(materials){
		materials.preload();
		var material = materials.materials['PersianCarpet'];
		var objectNew = object.clone();
		objectNew.children[0].material = material;
		global.models.props.rug2 = objectNew;

	mtlLoader.load('engine/models/obj-mtl/props/sofa.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/props/sofa.obj', function(object){
			//special attributes
			global.models.props.sofa = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/props/hypno.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/props/hypno.obj', function(object){
			//special attributes
			global.models.props.hypno = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/props/drainCleaner.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/props/drainCleaner.obj', function(object){
			//special attributes
			global.models.props.drainCleaner = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	objLoader.load('engine/models/obj-mtl/props/stone.obj', function(object){
		var material = new THREE.MeshLambertMaterial({color:0x777777});
		for(var i=0; i<object.children.length; i++){
			object.children[i].material = material;
		};
		//special attributes
		global.models.props.stone = object;
		menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/props/sword.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/props/sword.obj', function(object){
			object.children[5].visible = false;
			global.models.props.sword = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/props/billboard.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/props/billboard.obj', function(object){
			//special attributes
			global.models.props.billboard = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	objLoader.load('engine/models/obj-mtl/props/mud.obj', function(object){
		var material = new THREE.MeshLambertMaterial({color:0x825b34});
		for(var i=0; i<object.children.length; i++){
			object.children[i].material = material;
		};
		//special attributes
		global.models.props.mud = object;
		menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/props/car.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/props/car.obj', function(object){
			object.children[2].material = new THREE.MeshLambertMaterial({color:0xffffff, transparent: true, opacity: 0.2});
			global.models.props.car = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/props/tree.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/props/tree.obj', function(object){
			//special attributes
			global.models.props.tree = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/props/well.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/props/well.obj', function(object){
			object.children[1].material = object.children[0].material;
			object.children[2].material = object.children[0].material;
			global.models.props.well = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	txLoader.load('engine/models/obj-mtl/props/textures/moon_map_mercator.jpg', function(texture){
		var g1 = new THREE.SphereGeometry(200, 20, 20);
		var m1 = new THREE.MeshBasicMaterial({ map: texture, overdraw: 0.5 });
		var me1 = new THREE.Mesh(g1, m1);
		global.models.props.moon = me1;
		menuHelper.loadingMenu.updateProgress(numModels);

	txLoader.load('engine/models/obj-mtl/props/textures/land_ocean_ice_cloud_2048.jpg', function(texture){
		var g1 = new THREE.SphereGeometry(200, 20, 20);
		var m1 = new THREE.MeshBasicMaterial({ map: texture, overdraw: 0.5 });
		var me1 = new THREE.Mesh(g1, m1);
		global.models.props.earth = me1;
		menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/props/toilet.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/props/toilet.obj', function(object){
			//special attributes
			global.models.props.toilet = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/characters/matthew_stand.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/characters/matthew_stand.obj', function(object){
			object.children[7].material.side = THREE.DoubleSide;
			global.models.characters.matthew_stand = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/characters/matthew_pose.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/characters/matthew_pose.obj', function(object){
			object.children[7].material.side = THREE.DoubleSide;
			global.models.characters.matthew_pose = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/characters/stevie_stand.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/characters/stevie_stand.obj', function(object){
			object.children[0].material.transparent = true;
			object.children[6].material.transparent = true;
			global.models.characters.stevie_stand = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/characters/stevie_sit.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/characters/stevie_sit.obj', function(object){
			object.children[0].material.transparent = true;
			object.children[6].material.transparent = true;
			global.models.characters.stevie_sit = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/characters/quinn_stand.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/characters/quinn_stand.obj', function(object){
			object.children[6].material.side = THREE.DoubleSide;
			global.models.characters.quinn_stand = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/characters/quinn_pose.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/characters/quinn_pose.obj', function(object){
			object.children[6].material.side = THREE.DoubleSide;
			global.models.characters.quinn_pose = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/characters/ryan_stand.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/characters/ryan_stand.obj', function(object){
			object.children[4].material.transparent = true;
			global.models.characters.ryan_stand = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	mtlLoader.load('engine/models/obj-mtl/characters/miser_stand.mtl', function(materials){
		materials.preload();
		objLoader.setMaterials(materials);
		objLoader.load( 'engine/models/obj-mtl/characters/miser_stand.obj', function(object){
			object.children[1].material.side = THREE.DoubleSide;
			object.children[2].material.side = THREE.DoubleSide;
			object.children[5].scale.set(1,1,.93);
			global.models.characters.miser_stand = object;
			menuHelper.loadingMenu.updateProgress(numModels);

	//Launch Character Selection		
	console.log('Loaded '+menuHelper.loadingMenu.modelsLoaded.toString()+' Models.');
	console.log('Time Elapsed: '+(performance.now()*.001-global.loadingStartTime).toString()+' s.');
	console.info('Finished Loading Models.');
	launch(selectUserInit);
		});
	});
		});
		});
	});
		});
	});
		});
	});
		});
	});
		});
	});
		});
	});
		});
	});
		});
	});
	});
	});
		});
	});
		});
	});
		});
	});
	});
		});
	});
		});
	});
	});
		});
	});
		});
	});
		});
	});
	});
		});
	});
	});
		});
	});
	});
	});
		});
	});
		});
	});
		});
	});
	});
	});
	});
	});
	});
	});
	});
		});
	});
		});
	});
		});
	});
		});
	});
		});
	});
		});
	});
	});		
		});
	});
	});
	});
	});
	});
	});
	});
	});
	});
	});
	});
	});
	});
	});
	});
	});
	});
	});
	});
	});
};

selectUserDestroy = function(){
	menuHelper.characterSelectionMenu.hide();
	global.cameraControls.dispose();
	delete global.cameraControls;
	global.unlockableHelper.dispose();
	delete global.unlockableHelper;
	delete global.characterSelectionHelper;
	while(scene.children.length > 0){
		scene.remove(scene.children[0]); 
	};
	camera.position.set(0,0,0);
	camera.rotation.set(0,0,0);
	console.info('Ready for Next Scene.');
};

selectUserAnimate = function(){
	if(typeof global.cameraControls != 'undefined'){
		global.cameraControls.update();
	};
	if(typeof global.characterSelectionHelper != 'undefined'){
		global.characterSelectionHelper.update();
	};
};

selectUserInit = function(){
	animateLoop = selectUserAnimate;
	destroyFunction = selectUserDestroy;
	
	var root = '';

	console.info('Setting Up Character Selection Scene...');

	menuHelper.cover.show();
	menuHelper.characterSelectionMenu.show();
	setTimeout(function(){menuHelper.cover.hide();}, 10000);

	var light = new THREE.HemisphereLight(0x97d3db, 0x4c2d00, 2);
	scene.add(light);

	camera.position.set(0,.9,1.3);
	global.cameraControls = new THREE.OrbitControls(camera, renderer.domElement);
	global.cameraControls.target = new THREE.Vector3(0,.9,0);
	global.cameraControls.enablePan = false;
	global.cameraControls.enableZoom = false;

	global.characterSelectionHelper = new CharacterSelectionHelper();
	global.unlockableHelper = new CharacterUnlockablesHelper();
	
	var skyBox = global.models.structures.roomSphere.clone();
	scene.add(skyBox);
};

miserMainSceneDestroy = function(){
	menuHelper.audioPlayerMenu.hide();
	menuHelper.switchCharacterMenu.hide();
	menuHelper.muteMenu.hide();
	menuHelper.tutorialMenu.hide();
	menuHelper.interactionMenu.hide();
	menuHelper.completedMenu.hide();
	global.playerControls.dispose();
	delete global.playerControls;
	global.unlockHelper.dispose();
	delete global.unlockHelper;
	global.interactionHelper.dispose();
	delete global.interactionHelper;
	delete global.lightingHelper;
	delete global.songLocationHelper;
	global.billboardVideo.src = '';
	global.billboardVideo.pause();
	global.terreVideo.src = '';
	global.terreVideo.pause();
	delete global.billboardVideo;
	delete global.terreVideo;
	delete global.billboardVideoCanvas;
	delete global.billboardVideoTexture;
	delete global.terreVideoCanvas;
	delete global.terreVideoTexture;
	while(scene.children.length > 0){
		scene.remove(scene.children[0]); 
	};
	camera.position.set(0,0,0);
	camera.rotation.set(0,0,0);
	console.info('Ready For Next Scene.');
};

miserMainSceneAnimate = function(){
	var now = performance.now()*.001;
	if(typeof global.playerControls != 'undefined'){
		global.playerControls.update();
	};
	if(typeof global.lightingHelper != 'undefined'){
		global.lightingHelper.update();
	};
	if(typeof global.songLocationHelper != 'undefined'){
		global.songLocationHelper.update();
	};
	if(typeof global.interactionHelper != 'undefined'){
		global.interactionHelper.update();
	};
	if(typeof audio != 'undefined'){
		audio.musicTrack.update();
	};
	if(typeof menuHelper != 'undefined'){
		menuHelper.audioPlayerMenu.update();
	};
	if(typeof global.billboardVideo != 'undefined'){
		if(global.billboardVideo.readyState === global.billboardVideo.HAVE_ENOUGH_DATA){
    		if(global.billboardVideoCanvas.getContext('2d')){global.billboardVideoCanvas.getContext('2d').drawImage(global.billboardVideo, 0, 0)};
    		if(global.billboardVideoTexture){global.billboardVideoTexture.needsUpdate = true};
  		};
	};
	if(typeof global.terreVideo != 'undefined'){
		if(global.terreVideo.readyState === global.terreVideo.HAVE_ENOUGH_DATA){
    		if(global.terreVideoCanvas.getContext('2d')){global.terreVideoCanvas.getContext('2d').drawImage(global.terreVideo, 0, 0)};
    		if(global.terreVideoTexture){global.terreVideoTexture.needsUpdate = true};
  		};
	};
	if(typeof scene.getObjectByName('hypno') != 'undefined'){
		scene.getObjectByName('hypno').rotation.x = (now*(2*Math.PI))%(2*Math.PI);
	};
	if(typeof scene.getObjectByName('strobeLight') != 'undefined'){
		scene.getObjectByName('strobeLight').intensity= strobe(Math.PI*now*20)*10;
	};
	if(typeof scene.getObjectByName('drainCleaner') != 'undefined'){
		scene.getObjectByName('drainCleaner').lookAt(global.playerControls.position);
	};
	if(typeof scene.getObjectByName('car') != 'undefined'){
		scene.getObjectByName('car').rotation.x = Math.sin((Math.PI*now)%(2*Math.PI))*.1;
		scene.getObjectByName('car').rotation.z = Math.sin((.8*Math.PI*now)%(2*Math.PI))*.1;
	};
	if(typeof scene.getObjectByName('earth') != 'undefined'){
		scene.getObjectByName("earth").rotation.y = .15*now;
	};
	if(typeof scene.getObjectByName('moon') != 'undefined'){
		scene.getObjectByName("moon").position.set(150*Math.sin(.5*Math.PI*now)-23.5, 60, 150*Math.cos(.5*Math.PI*now)-299);
	};
};

miserMainSceneInit = function(){
	animateLoop = miserMainSceneAnimate;
	destroyFunction = miserMainSceneDestroy;

	console.info('Setting Up Main Scene...');

	var root = '/themiserinteractive/';
	
	menuHelper.cover.show();
	menuHelper.audioPlayerMenu.show();
	menuHelper.switchCharacterMenu.show();
	menuHelper.muteMenu.show();
	menuHelper.tutorialMenu.show();
	menuHelper.audioPlayerMenu.unlockTrack(0);
	menuHelper.audioPlayerMenu.unlockTrack(3);
	menuHelper.audioPlayerMenu.unlockTrack(6);

	setTimeout(function(){menuHelper.cover.hide(); menuHelper.tutorialMenu.playNarration();}, 10000);
	
	var playerModel;
	switch(menuHelper.characterSelectionMenu.currentCharacter){
		case 0:
			playerModel = global.models.characters.matthew_stand.clone();
			playerModel.scale.set(7.2,7.2,7.2);
			break;
		case 1:
			playerModel = global.models.characters.stevie_stand.clone();
			playerModel.scale.set(6.8,6.8,6.8);
			break;
		case 2:
			playerModel = global.models.characters.quinn_stand.clone();
			playerModel.scale.set(6.8,6.9,6.8);
			break;
		case 3:
			playerModel = global.models.characters.ryan_stand.clone();
			playerModel.scale.set(6.7,6.7,6.7);
			break;
		case 4:
			playerModel = global.models.characters.miser_stand.clone();
			playerModel.scale.set(6.5,6.5,6.5);
			break;
	};
	playerModel.name = 'playerModel';
	scene.add(playerModel);

	global.playerControls = new MiserControls(camera,scene.getObjectByName('playerModel'));
	global.playerControls.followRadius = 3; global.playerControls.meshFollowOffset.set(0,-11,0); global.playerControls.meshThetaOffset = Math.PI; global.playerControls.velocity = 20; 	global.playerControls.speed = 3;
	global.playerControls.position.set(86,10,-16);
	global.playerControls.rotation.set(0,2.5847963267948897,0);
	global.playerControls.update();
	global.playerControls.enabled = false;

	global.unlockHelper = new GameUnlockablesHelper();
	global.lightingHelper = new LightingHelper();
	global.songLocationHelper = new SongLocationHelper();
	global.interactionHelper = new InteractionHelper();

	//Lights
	var light1 = new THREE.PointLight(0xffffff, .5, 100);
	light1.position.set(56, 20, -6);
	scene.add(light1);

	var light2 = new THREE.PointLight( 0xff0000, 5, 30 );
	light2.position.set(50, 5, 30);
	scene.add(light2);
	
	var light3 = new THREE.PointLight( 0x0090ff, 5, 30 );
	light3.position.set(46.6, 5, 30);
	scene.add(light3);

	var light4 = new THREE.PointLight(0xff0000, 2.7, 50);
	light4.position.set(21, 10, 16);
	scene.add(light4);

	//Interactive Lights
	var light5 = new THREE.HemisphereLight(0xffffbb, 0x080820, 1 );
	light5.name = "hemisphereLight";
	scene.add(light5);

	light6 = new THREE.PointLight(0xffffff, 10, 37, 0);
	light6.name = "strobeLight";
	light6.position.set(-18.7, 10, -12.7);
	scene.add(light6);

	//Scene
	//Stars
	var starRadius = 1000;
	var maxStarSize = 3;
	var numStars = 2000;
	var offset = {x:-23.5, y:0, z:-299};
	for (var i=0; i<numStars; i++){
		var geo = new THREE.SphereGeometry( Math.random()*maxStarSize, 4, 4 );
		var mat = new THREE.MeshBasicMaterial( {color: 0xffffff} );
		var rot = {theta:Math.random()*2*Math.PI, phi:Math.random()*Math.PI-Math.PI/2};
		var pos = new THREE.Vector3();
		pos.x = starRadius*Math.sin(rot.theta)*Math.sin(rot.phi+Math.PI/2)+offset.x;
		pos.y = starRadius*Math.cos(rot.phi+Math.PI/2)+offset.y;
		pos.z = starRadius*Math.cos(rot.theta)*Math.sin(rot.phi+Math.PI/2)+offset.z;
		var star = new THREE.Mesh( geo, mat );
		star.position.set(pos.x,pos.y,pos.z);
		scene.add(star);
	};

	//4th Dimension
	var ground4d = new THREE.GridHelper( 250, 5, 0x002b77, 0x002b77);
	ground4d.position.set(-23.5,0,-299);
	scene.add( ground4d );

	var portalOut = new THREE.GridHelper( 8, .1, 0xffffff, 0xffffff);
	portalOut.position.set(-23.5,8,-48.5);
	portalOut.rotation.set(Math.PI/2,0,0);
	scene.add(portalOut);

	var portalIn = new THREE.GridHelper( 8, .1, 0xffffff, 0xffffff);
	portalIn.position.set(16.7,8,-9);
	portalIn.rotation.set(0,0,Math.PI/2);
	scene.add(portalIn);

	for(var i=0; i<11; i++){
		if (i==4){
			var wallGeo = new THREE.PlaneGeometry(42,50);
			var wallMaterial = new THREE.MeshBasicMaterial( {color: 0x0, side: THREE.DoubleSide} );
			var wall = new THREE.Mesh(wallGeo, wallMaterial);
			wall.position.set(5.5,25,-48.5);
			scene.add(wall);
			continue;
		}
		else if (i==5){
			var wallGeo = new THREE.PlaneGeometry(42,50);
			var wallMaterial = new THREE.MeshBasicMaterial( {color: 0x0, side: THREE.DoubleSide} );
			var wall = new THREE.Mesh(wallGeo, wallMaterial);
			wall.position.set(-52.5,25,-48.5);
			scene.add(wall);
			continue;
		}
		else if (i==10){
			var wallGeo = new THREE.PlaneGeometry(16,34);
			var wallMaterial = new THREE.MeshBasicMaterial( {color: 0x0, side: THREE.DoubleSide} );
			var wall = new THREE.Mesh(wallGeo, wallMaterial);
			wall.position.set(-23.5,33,-48.5);
			scene.add(wall);
			continue;
		};
		var wallGeo = new THREE.PlaneGeometry(50,50);
		var wallMaterial = new THREE.MeshBasicMaterial( {color: 0x0, side: THREE.DoubleSide} );
		var wall = new THREE.Mesh(wallGeo, wallMaterial);
		wall.position.set(201.5-i*50,25,-48.5);
		scene.add(wall);
	};

	//Basement
	var room = global.models.structures.basement.clone();
	room.scale.set(.04,.04,.04);
	scene.add(room);

	//Bathroom
	var bathroom = global.models.structures.bathroom.clone();
	bathroom.scale.set(1,1,1);
	bathroom.position.set(0, -1000, 5000);
	bathroom.rotation.set(-Math.PI/2,0,0);
	scene.add(bathroom);

	//Animated Props
	var hypno = global.models.props.hypno.clone();
	hypno.position.set(-30,8,-9);
	hypno.scale.set(7,7,7);
	hypno.rotation.set(0, Math.PI/2, 0);
	hypno.name = "hypno";
	scene.add(hypno);

	var drainCleaner = global.models.props.drainCleaner.clone();
	drainCleaner.scale.set(100,100,100);
	drainCleaner.position.set(-254,21,-100);
	drainCleaner.rotation.set(0, Math.PI/2, 0);
	drainCleaner.name = "drainCleaner";
	scene.add(drainCleaner)

	var sword = global.models.props.sword.clone();
	sword.scale.set(.4,.4,.4);
	sword.position.set(-240,16,-240);
	sword.rotation.set(0,0,-Math.PI/2);
	sword.name = 'sword';
	scene.add(sword);

	var car = global.models.props.car.clone();
	car.scale.set(.2,.2,.2);
	car.position.set(185,0,-222);
	car.rotation.order = "YXZ";
	car.rotation.set(-.2, -Math.PI/4, 0);
	car.name = 'car';
	scene.add(car);

	var earth = global.models.props.earth.clone();
	earth.scale.set(.3,.3,.3);
	earth.position.set(-23.5,60,-299);
	earth.name = "earth";
	scene.add( earth);

	var moon = global.models.props.moon.clone();
	moon.scale.set(.03,.03,.03);
	moon.position.set(-23.5,60,-150);
	moon.name = "moon";
	scene.add( moon);

	var songMtl = new THREE.MeshLambertMaterial({color: 0xffffff});
	var song2Geo = new THREE.TextGeometry('Drano', {font:global.fonts.droidSansBold, size:1, height:.5});
	song2Geo.center();
	var song2 = new THREE.Mesh(song2Geo, songMtl);
	song2.name = 'song2';
	var song3Geo = new THREE.TextGeometry('The Miser', {font:global.fonts.droidSansBold, size:1, height:.5});
	song3Geo.center();
	var song3 = new THREE.Mesh(song3Geo, songMtl);
	song3.name = 'song3';	
	var song5Geo = new THREE.TextGeometry('Lovely Trip', {font:global.fonts.droidSansBold, size:1, height:.5});
	song5Geo.center();
	var song5 = new THREE.Mesh(song5Geo, songMtl);
	song5.name = 'song5';
	var song6Geo = new THREE.TextGeometry('Acid Face', {font:global.fonts.droidSansBold, size:1, height:.5});
	song6Geo.center();
	var song6 = new THREE.Mesh(song6Geo, songMtl);
	song6.name = 'song6';
	var song8Geo = new THREE.TextGeometry('Eviscerator', {font:global.fonts.droidSansBold, size:1, height:.5});
	song8Geo.center();
	var song8 = new THREE.Mesh(song8Geo, songMtl);
	song8.name = 'song8';
	var song9Geo = new THREE.TextGeometry('Minds Eye', {font:global.fonts.droidSansBold, size:1, height:.5});
	song9Geo.center();
	var song9 = new THREE.Mesh(song9Geo, songMtl);
	song9.name = 'song9';
	var song10Geo = new THREE.TextGeometry('Rut', {font:global.fonts.droidSansBold, size:1, height:.5});
	song10Geo.center();
	var song10 = new THREE.Mesh(song10Geo, songMtl);
	song10.name = 'song10';
	song2.position.set(-261,10,-70);
	song3.position.set(210.2,10,-416);
	song5.position.set(-27,10,-360);
	song6.position.set(155,11,-129);
	song8.position.set(-253,10,-266);
	song9.position.set(-205,10,-531);
	song10.position.set(209,10,-211);
	scene.add(song2); scene.add(song3); scene.add(song5); scene.add(song6); scene.add(song8); scene.add(song9); scene.add(song10);

	//Video Screens
	global.billboardVideo = document.createElement('video'); global.billboardVideo.src = global.video.billboardVideoURL; global.billboardVideo.loop = true;
	//global.billboardVideo.setAttribute('crossorigin', 'anonymous');
	global.billboardVideo.load();
	global.billboardVideo.play();
	global.billboardVideoCanvas = document.createElement('canvas');
	global.billboardVideoCanvas.width = 1024;
	global.billboardVideoCanvas.height = 512;
	global.billboardVideoCanvas.getContext('2d').fillStyle = "#000000";
	global.billboardVideoCanvas.getContext('2d').fillRect(0,0,1024,512);
	global.billboardVideoTexture = new THREE.Texture(global.billboardVideoCanvas);
	global.billboardVideoTexture.minFilter = THREE.LinearFilter;
	global.billboardVideoTexture.magFilter = THREE.LinearFilter;
	var billboardScreenMaterial = new THREE.MeshBasicMaterial( { map: global.billboardVideoTexture, overdraw:true, side:THREE.DoubleSide } );
	var billboardScreenGeometry = new THREE.PlaneGeometry(102.4,51.2);
	var billboardScreenMesh = new THREE.Mesh(billboardScreenGeometry, billboardScreenMaterial);
	billboardScreenMesh.position.set(132,67,-450.5);
	billboardScreenMesh.scale.set(1.6,1.6,1.6);
	billboardScreenMesh.rotation.set(0,-Math.PI/6,0);
	billboardScreenMesh.name="billBoardScreen";
	scene.add(billboardScreenMesh);

	global.terreVideo = document.createElement('video'); global.terreVideo.src = global.video.terreVideoURL; global.terreVideo.loop = true;
	//global.terreVideo.setAttribute('crossorigin', 'anonymous');
	global.terreVideo.load();
	global.terreVideoCanvas = document.createElement('canvas');
	global.terreVideoCanvas.width = 480;
	global.terreVideoCanvas.height = 360;
	global.terreVideoCanvas.getContext('2d').fillStyle = "#000000";
	global.terreVideoCanvas.getContext('2d').fillRect(0,0,480,360);
	global.terreVideoTexture = new THREE.Texture(global.terreVideoCanvas);
	global.terreVideoTexture.minFilter = THREE.LinearFilter;
	global.terreVideoTexture.magFilter = THREE.LinearFilter;
	var terreScreenMaterial = new THREE.MeshBasicMaterial( { map: global.terreVideoTexture, overdraw:true, side:THREE.DoubleSide } );
	var terreScreenGeometry = new THREE.PlaneGeometry(48,36);
	var terreScreenMesh = new THREE.Mesh(terreScreenGeometry, terreScreenMaterial);
	terreScreenMesh.position.set(0,10,6300);
	terreScreenMesh.scale.set(3,3,3);
	terreScreenMesh.rotation.set(0,Math.PI,0);
	terreScreenMesh.name="terreScreen";
	scene.add(terreScreenMesh);

	//Basement Props
	var drums = global.models.props.drums.clone();
	drums.scale.set(.05,.05,.05);
	drums.position.set(82,5,15);
	scene.add(drums);

	var amp1 = global.models.props.guitarAmp.clone();
	amp1.scale.set(6,6,6);
	amp1.position.set(91,0,17);
	amp1.rotation.set(0, 5*Math.PI/4, 0);
	var amp2 = amp1.clone();
	amp2.position.set(93,0,10);
	amp2.rotation.set(0, 6*Math.PI/4,0);
	scene.add(amp1); scene.add(amp2);

	var gtr1 = global.models.props.guitarCream.clone();
	gtr1.scale.set(.05,.05,.05);
	gtr1.position.set(92,-3.3,10.52);
	gtr1.lookAt(new THREE.Vector3(0,50,10.52));
	var gtr2 = global.models.props.guitarBlack.clone();
	gtr2.scale.set(.05,.05,.05);
	gtr2.position.set(85,-4,15);
	gtr2.rotation.set(-Math.PI/2,0,0);
	scene.add(gtr1, gtr2);

	var album = global.models.props.album.clone();
	album.position.set(83.5,7,32.2);
	album.scale.set(.05,.05,.05);
	album.rotation.set(-.09,Math.PI,0);
	var album2 = global.models.props.album2.clone();
	album2.scale.set(.05,.1,.05);
	album2.position.set(57,3,32.2);
	album2.rotation.set(-.09,Math.PI,0);
	var album3 = global.models.props.album3.clone();
	album3.scale.set(.1,.1,.05);
	album3.position.set(96.2,12,-19);
	album3.rotation.order = "YXZ";
	album3.rotation.set(.09,-Math.PI/2,0);
	var album4 = global.models.props.album4.clone();
	album4.scale.set(.1,.07,.05);
	album4.position.set(96.2,5,-9);
	album4.rotation.order = "YXZ";
	album4.rotation.set(.16,-Math.PI/2,0);
	var album5 = global.models.props.album5.clone();
	album5.scale.set(.28,.28,.1);
	album5.position.set(63,2,-33.3);
	album5.rotation.order = "YXZ";
	album5.rotation.set(.06,0,0);
	var album6 = global.models.props.album6.clone();
	album6.scale.set(.05,.05,.05);
	album6.position.set(16,7,3);
	album6.rotation.order = "YXZ";
	album6.rotation.set(.09,Math.PI/2,0);
	var album7 = global.models.props.album7.clone();
	album7.scale.set(1,2,1);
	album7.position.set(-680, -110, 4900);
	album7.name = 'album7';
	var album8 = global.models.props.album8.clone();
	album8.scale.set(2,2,1);
	album8.position.set(-480, -110, 4900);
	album8.name = 'album8';
	scene.add(album); scene.add(album2); scene.add(album3); scene.add(album4); scene.add(album5); scene.add(album6); scene.add(album7); scene.add(album8);

	var bass = global.models.props.bass.clone();
	bass.position.set(72.7,3,25.6);
	bass.scale.set(2.1,2.1,2.1);
	bass.rotation.set(.2,Math.PI,1.0472);
	scene.add(bass);

	var bassHead = global.models.props.bassHead.clone();
	bassHead.scale.set(.42,.25,.2);
	bassHead.position.set(76.1,9.3,26.8);
	bassHead.rotation.set(0,Math.PI,0);
	var bassCab = global.models.props.bassCab.clone();
	bassCab.scale.set(12,12,12);
	bassCab.position.set(74,0,15);
	bassCab.rotation.set(0,Math.PI,0);
	scene.add(bassHead); scene.add(bassCab);

	var bbR = global.models.props.beanBagRed.clone();
	bbR.scale.set(8,8,8);
	bbR.position.set(66,0,11);
	bbR.rotation.set(0, 5*Math.PI/4, 0);
	var bbP = global.models.props.beanBagPurple.clone();
	bbP.position.set(43.5,0,7.5);
	bbP.scale.set(8,8,8);
	bbP.rotation.set(0,0,0);
	var bbY = global.models.props.beanBagYellow.clone();
	bbY.position.set(66.3,0,26.6);
	bbY.scale.set(8,8,8);
	bbY.rotation.set(0,2*Math.PI/3,0);
	scene.add(bbY, bbR, bbP);

	var rug = global.models.props.rug.clone();
	rug.scale.set(.2,.2,.2);
	rug.position.set(56,0,15);
	rug.rotation.set(0, 0, 0);
	var rug2 = global.models.props.rug2.clone();
	rug2.scale.set(.3,.2,.2);
	rug2.position.set(73,0,-15);
	rug2.rotation.set(0, 0, 0);
	scene.add(rug); scene.add(rug2);

	var redLavaLamp = global.models.props.lavaLampSmallRed.clone();
	redLavaLamp.scale.set(.5,.5,.5);
	redLavaLamp.position.set(50,1,28.7);
	redLavaLamp.rotation.set(0,2*Math.PI/3,0);
	var blueLavaLamp = global.models.props.lavaLampLargeBlue.clone();
	blueLavaLamp.scale.set(.8,.8,.8);
	blueLavaLamp.position.set(46.6,2,28.7);
	blueLavaLamp.rotation.set(0,2*Math.PI/3,0);
	scene.add(redLavaLamp); scene.add(blueLavaLamp);

	var speaker1 = global.models.props.stereoSpeaker.clone();
	speaker1.scale.set(.25,.25,.25);
	speaker1.position.set(23,-4,25);
	speaker1.rotation.set(0, -Math.PI/2, 0);
	var speaker2 = speaker1.clone();
	speaker2.position.set(18,-4,24);
	scene.add(speaker1, speaker2);

	var floorLamp = global.models.props.floorLamp.clone();
	floorLamp.scale.set(.0025,.0025,.0025);
	floorLamp.position.set(21,0,16);
	floorLamp.rotation.set(-Math.PI/2,0,0);
	scene.add(floorLamp);

	var sofa = global.models.props.sofa.clone();
	sofa.scale.set(.08,.08,.08);
	sofa.position.set(56,0,12);
	sofa.rotation.set(0, 0, 0);
	scene.add(sofa);

	//Bathroom Props
	var toilet = global.models.props.toilet.clone();
	toilet.scale.set(.4,.4,.4);
	toilet.position.set(-600, -1000, 5225);
	scene.add(toilet);

	//4d Props
	var stone = global.models.props.stone.clone();
	stone.scale.set(10,10,10);
	stone.position.set(-240,3,-246);
	scene.add(stone);

	var pyramid = global.models.structures.pyramid.clone();
	pyramid.scale.set(.7,.7,.7);
	pyramid.position.set(-175,0,-450);
	scene.add(pyramid);

	var billboard = global.models.props.billboard.clone();
	billboard.scale.set(10,10,10);
	billboard.position.set(170,0,-518);
	billboard.rotation.set(0,-Math.PI/6,0);
	scene.add(billboard);

	var bGeo = new THREE.CubeGeometry(55,5,55);
	var bMat = new THREE.MeshBasicMaterial( {color: 0x0, side: THREE.DoubleSide} );
	var bBox1 = new THREE.Mesh(bGeo, bMat);
	bBox1.scale.set(4,1,4);
	bBox1.position.set(157,-2.6,-130);
	var bBox2 = bBox1.clone();
	bBox2.scale.set(1,1,1);
	bBox2.position.set(185,-2.6,-222);
	var mud1 = global.models.props.mud.clone();
	var mud2 = mud1.clone();
	mud1.scale.set(.000004,.0000005,.000004);
	mud1.position.set(157,-1.1,-130);
	mud2.position.set(185,-1.2,-222);
	mud2.scale.set(.000001,.0000005,.000001);
	scene.add(bBox1); scene.add(bBox2); scene.add(mud1); scene.add(mud2);

	var well = global.models.props.well.clone();
	well.scale.set(10,10,10);
	well.position.set(155.5,3,-130);
	well.rotation.set(0,Math.PI/2,0);
	scene.add(well);

	//Forest
	var tree = global.models.props.tree.clone();
	var numTrees = 500;
	for (var i=0; i<numTrees; i++){
		var coordsValid = false;
		var randomCoords;
		do{
			randomCoords = {x:Math.random()*111+100, z:Math.random()*130-195};
			if(Math.sqrt((randomCoords.z+130)*(randomCoords.z+130)+(randomCoords.x-155.5)*(randomCoords.x-155.5))<=18.5){
				coordsValid = false;
			}
			else if(randomCoords.x<=145){
				if(randomCoords.z>=-143 && randomCoords.z<=-117){
					coordsValid = false;
				}
				else{
					coordsValid = true;
				}
			}
			else{
				coordsValid = true;
			}
		}
		while(!coordsValid);
		var size = {x:Math.random()*4+1,y:Math.random()*9+1};
		var newTree = tree.clone();
		newTree.position.set(randomCoords.x,2*size.y,randomCoords.z);
		newTree.scale.set(size.x,size.y,size.x);
		scene.add(newTree);
	};
};