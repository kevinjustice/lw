/*
Kevin R. Justice
022717
MiserControls.js

camera rotation controls. Attaches mesh to frame that moves with camera angle
*/

MiserControls = function (camera, mesh) {

	var scope = this;
	var movementDirection = {f: 0, b: 0, l: 0, r:0};
	var trackingDirection = {l:0, r:0};
	var tRecent = performance.now()*.001;

	var collisionRegionsIndoors = [
		{p1: new THREE.Vector3( 90.5, 0, 35), p2: new THREE.Vector3( 100.5, 0, -35)}, //Amp Wall
		{p1: new THREE.Vector3( 15.299999999999997, 0, -27.5), p2: new THREE.Vector3( 95.3, 0, -34.4)}, //Stair Wall
		{p1: new THREE.Vector3( 15.299999999999997, 0, 34.6), p2: new THREE.Vector3( 95.3, 0, 26)}, //Lavalamp Wall
		{p1: new THREE.Vector3( 15, 0, 32.4), p2: new THREE.Vector3( 21, 0, -2.5999999999999996)}, //portal wallLeft
		{p1: new THREE.Vector3( 15, 0, -13), p2: new THREE.Vector3( 21, 0, -33)}, //portal wallRight
		{p1: new THREE.Vector3( -33.5, 0, 2.2), p2: new THREE.Vector3( 21.5, 0, -5)}, //portal tunnel left
		{p1: new THREE.Vector3( -18.5, 0, -12), p2: new THREE.Vector3( 18.5, 0, -18)}, //portal tunnel right
		{p1: new THREE.Vector3( -35.2, 0, 0.5), p2: new THREE.Vector3( -26.2, 0, -52.5)}, //portaltunnel portal wall
		{p1: new THREE.Vector3( -19.5, 0, -16.5), p2: new THREE.Vector3( -12.5, 0, -51.5)}, //portaltunnel opposite wall
		{p1: new THREE.Vector3( 38, 0, 6.5), p2: new THREE.Vector3( 73, 0, -3.5)}, //couch
		{p1: new THREE.Vector3( 65.2, 0, 32.5), p2: new THREE.Vector3( 100.2, 0, 17.5)}, //music equipment
		{p1: new THREE.Vector3( 15, 0, 30.5), p2: new THREE.Vector3( 52.7, 0, 23.5)}, //lavalamp wall lavalampsspkr
		{p1: new THREE.Vector3( 18.700000000000003, 0, -13), p2: new THREE.Vector3( 48.7, 0, -34)} //Stairs
	];

	var collisionRegionsOutdoors = [
		{p1: new THREE.Vector3( -35.2, 0, 0.5), p2: new THREE.Vector3( -26.2, 0, -52.5)}, //portaltunnel portal wall
		{p1: new THREE.Vector3( -18.5, 0, -16.5), p2: new THREE.Vector3( -12.5, 0, -51.5)}, //portaltunnel opposite wall
		{p1: new THREE.Vector3( -278.5, 0, -45.5), p2: new THREE.Vector3( -28.5, 0, -52.5)}, //4d wall1
		{p1: new THREE.Vector3( -18.5, 0, -45.5), p2: new THREE.Vector3( 231.5, 0, -52.5)}, //4d wall2
		{p1: new THREE.Vector3( -278.5, 0, -546.5), p2: new THREE.Vector3( 231.5, 0, -553.5)}, //4d wall3
		{p1: new THREE.Vector3( 221, 0, -44), p2: new THREE.Vector3( 228, 0, -554)}, //4d wall4
		{p1: new THREE.Vector3( -276, 0, -44), p2: new THREE.Vector3( -269, 0, -554)}, //4d wall5
		{p1: new THREE.Vector3( -255.84, 0, -226.5), p2: new THREE.Vector3( -220.84, 0, -261.5)}, //stone
		{p1: new THREE.Vector3( -255, 0, -370), p2: new THREE.Vector3( -95, 0, -530)}, //pyramid
		{p1: new THREE.Vector3( 70, 0, -428), p2: new THREE.Vector3( 240, 0, -548)}, // projector
		{p1: new THREE.Vector3( -61, 0, -261.5), p2: new THREE.Vector3( 14, 0, -336.5)}, // earth
		{p1: new THREE.Vector3( 164, 0, -202), p2: new THREE.Vector3( 206, 0, -242)}, // car
		{p1: new THREE.Vector3( 148, 0, -58), p2: new THREE.Vector3( 217, 0, -200)}, // forest back
		{p1: new THREE.Vector3( 97.5, 0, -136), p2: new THREE.Vector3( 166.5, 0, -196)}, // forest left
		{p1: new THREE.Vector3( 97.5, 0, -62), p2: new THREE.Vector3( 166.5, 0, -122)} // forest right
	];

	this.enabled = true;
	this.movementEnabled = true;

	this.position = new THREE.Vector3(0,0,0);
	this.rotation = new THREE.Euler(0,0,0);

	//Be careful with x and z. they will make path elliptical if model is centered
	this.meshFollowOffset = new THREE.Vector3(0,0,0);

	//Whatever faces +Z by default will face the camera unless meshThetaoffset is set.
	this.meshThetaOffset = 0;
	this.followRadius = 1;
	this.followTheta = 0;

	this.speed = 1;
	this.velocity = 1;

	var onKeyDown = function(e){
		if(e.keyCode == 87){
			movementDirection.f = 1;
		}
		else if(e.keyCode == 65){
			movementDirection.l = 1;
		}
		else if(e.keyCode == 83){
			movementDirection.b = 1;
		}
		else if(e.keyCode == 68){
			movementDirection.r = 1;
		}
		else if(e.keyCode == 39){
			trackingDirection.r = 1;
		}
		else if(e.keyCode == 37){
			trackingDirection.l = 1;
		};
	};

	var onKeyUp = function(e){
		if(e.keyCode == 87){
			movementDirection.f = 0;
		}
		else if(e.keyCode == 65){
			movementDirection.l = 0;
		}
		else if(e.keyCode == 83){
			movementDirection.b = 0;
		}
		else if(e.keyCode == 68){
			movementDirection.r = 0;
		}
		else if(e.keyCode == 39){
			trackingDirection.r = 0;
		}
		else if(e.keyCode == 37){
			trackingDirection.l = 0;
		};
	};

	this.addListeners = function(){
		window.addEventListener( 'keydown', onKeyDown, false);
		window.addEventListener( 'keyup', onKeyUp, false);
	};

	this.detectCollision = function(vector){
		var attempt = vector;
		var currentRegion = new function(){};
		if((attempt.z>-38.5 && attempt.z<43.5)){
			for(var i=0; i<collisionRegionsIndoors.length; i++){
				currentRegion.p1 = collisionRegionsIndoors[i].p1;
				currentRegion.p2 = collisionRegionsIndoors[i].p2;
				if((currentRegion.p1.x<vector.x && vector.x<currentRegion.p2.x) && (currentRegion.p2.z<vector.z && vector.z<currentRegion.p1.z)){
					return(true);
				};
			};
		}
		else{
			for(var i=0; i<collisionRegionsOutdoors.length; i++){
				currentRegion.p1 = collisionRegionsOutdoors[i].p1;
				currentRegion.p2 = collisionRegionsOutdoors[i].p2;
				if((currentRegion.p1.x<vector.x && vector.x<currentRegion.p2.x) && (currentRegion.p2.z<vector.z && vector.z<currentRegion.p1.z)){
					return(true);
				};
			};
		};
		return(false);
	};

	this.update = function(){
		if(scope.enabled){
			var intensity =  -trackingDirection.r+trackingDirection.l;
			var now = performance.now()*.001;
			var dT = now-tRecent;
			var dD = dT*scope.velocity;
			var dX, dZ;
			dX = Math.min(dD*((-movementDirection.f+movementDirection.b)*Math.sin(scope.rotation.y)+(movementDirection.r-movementDirection.l)*Math.cos(scope.rotation.y)), 1);
			dZ = Math.min(dD*((-movementDirection.f+movementDirection.b)*Math.cos(scope.rotation.y)+(-movementDirection.r+movementDirection.l)*Math.sin(scope.rotation.y)), 1);
			var dTheta = dT*scope.speed*intensity;
			if(scope.movementEnabled && !scope.detectCollision(new THREE.Vector3(scope.position.x+dX, 0, scope.position.z+dZ))){scope.position.set(scope.position.x+dX, scope.position.y, scope.position.z+dZ)};
			scope.rotation.y = (scope.rotation.y+dTheta)%(2*Math.PI);
			camera.position.set(scope.position.x, scope.position.y, scope.position.z);
			mesh.position.set(scope.followRadius*-Math.sin(scope.rotation.y+scope.followTheta)+scope.meshFollowOffset.x+scope.position.x, scope.meshFollowOffset.y+scope.position.y, scope.followRadius*-Math.cos(scope.rotation.y+scope.followTheta)+scope.meshFollowOffset.z+scope.position.z);
			camera.rotation.y = scope.rotation.y;
			mesh.rotation.y = (scope.rotation.y+scope.meshThetaOffset);	
		};
		tRecent = performance.now()*.001;
	}

	this.dispose = function(){
		window.removeEventListener( 'keydown', onKeyDown, false);
		window.removeEventListener( 'keyup', onKeyUp, false);
	};

	scope.addListeners();
	scope.update();
};
