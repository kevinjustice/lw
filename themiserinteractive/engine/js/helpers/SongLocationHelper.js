/*
Kevin R. Justice
04100917
SongAnimationHelper.js

helper for animating song location text in the 3d space
*/

SongLocationHelper = function(){
	var scope = this;

	this.update = function(){
		var now = performance.now()*.001;
		var ready = (typeof scene.getObjectByName('song2') != 'undefined') && (typeof scene.getObjectByName('song3') != 'undefined') && (typeof scene.getObjectByName('song5') != 'undefined') && (typeof scene.getObjectByName('song6') != 'undefined') && (typeof scene.getObjectByName('song8') != 'undefined') && (typeof scene.getObjectByName('song9') != 'undefined') && (typeof scene.getObjectByName('song10') != 'undefined');
		if(ready){
			scene.getObjectByName('song2').rotation.y = (2*Math.PI*now)%(2*Math.PI);
			scene.getObjectByName('song3').rotation.y = (2*Math.PI*now)%(2*Math.PI);
			scene.getObjectByName('song5').rotation.y = (2*Math.PI*now)%(2*Math.PI);
			scene.getObjectByName('song6').rotation.y = (2*Math.PI*now)%(2*Math.PI);
			scene.getObjectByName('song8').rotation.y = (2*Math.PI*now)%(2*Math.PI);
			scene.getObjectByName('song9').rotation.y = (2*Math.PI*now)%(2*Math.PI);
			scene.getObjectByName('song10').rotation.y = (2*Math.PI*now)%(2*Math.PI);
		};
	};
};