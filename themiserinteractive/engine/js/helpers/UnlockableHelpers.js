/*
Kevin R. Justice
040917
UnlockableHelpers.js

helper for unlocking in game content
*/

CharacterUnlockablesHelper = function (){
	var scope = this;

	var characterBuffer = [0,0,0,0,0];

	var onKeyUp = function(e){
		characterBuffer.shift();
		characterBuffer.push(e.keyCode);
		scope.check();
	};

	this.addListener = function(){
		window.addEventListener( 'keyup', onKeyUp, false);
	};

	this.check = function(){
		if(characterBuffer[0] == 77 && characterBuffer[1] == 73 && characterBuffer [2] == 83 && characterBuffer[3] == 69 && characterBuffer[4] == 82){
			menuHelper.characterSelectionMenu.activateMiser();
		};
	};

	this.dispose = function(){
		window.removeEventListener( 'keyup', onKeyUp, false);
	};

	scope.addListener();
};

GameUnlockablesHelper = function (){
	var scope = this;

	var characterBuffer = [0,0,0,0,0];
	var tracksFinished = [false,false,false,false,false,false,false,false,false,false];

	this.inEasterEgg = false;

	var onKeyUp = function(e){
		if(e.keyCode == 67){
			scope.collect();
		};
		if(e.keyCode == 76){
			scene.getObjectByName('hemisphereLight').visible = !scene.getObjectByName('hemisphereLight').visible;
		};
		characterBuffer.shift();
		characterBuffer.push(e.keyCode);
		scope.check();
	};

	this.addListener = function(){
		window.addEventListener( 'keyup', onKeyUp, false);
	};

	this.check = function(){
		if(characterBuffer[0] == 84 && characterBuffer[1] == 69 && characterBuffer [2] == 82 && characterBuffer[3] == 82 && characterBuffer[4] == 69){
			audio.sfxTrack.play(global.audio.sfx.miserUnlock);
			if(!scope.inEasterEgg){
				scope.inEasterEgg = true;
				global.billboardVideo.pause();
				audio.musicTrack.setVolume(0);
				audio.musicTrack.fadingIn = false;
				global.terreVideo.play();
				global.playerControls.position.set(0,10,6224);
				global.playerControls.movementEnabled = false;
				global.playerControls.rotation.set(0,0,0);
			}
			else{
				scope.inEasterEgg = false
				global.terreVideo.pause();
				if(!menuHelper.muteMenu.muted){
					audio.musicTrack.fadeIn();
				};
				global.billboardVideo.play();
				global.playerControls.position.set(86,10,-16);
				global.playerControls.movementEnabled = true;
				global.playerControls.rotation.set(0,2.585,0);
			};
		}
		else if(characterBuffer[0] == 71 && characterBuffer[1] == 73 && characterBuffer [2] == 77 && characterBuffer[3] == 77 && characterBuffer[4] == 69){
			audio.sfxTrack.play(global.audio.sfx.miserUnlock);
			if(!menuHelper.audioPlayerMenu.unlockedArray[0]){
				menuHelper.audioPlayerMenu.unlockTrack(0);
			};
			if(!menuHelper.audioPlayerMenu.unlockedArray[1]){
				menuHelper.audioPlayerMenu.unlockTrack(1);
			};
			if(!menuHelper.audioPlayerMenu.unlockedArray[2]){
				menuHelper.audioPlayerMenu.unlockTrack(2);
			};
			if(!menuHelper.audioPlayerMenu.unlockedArray[3]){
				menuHelper.audioPlayerMenu.unlockTrack(3);
			};
			if(!menuHelper.audioPlayerMenu.unlockedArray[4]){
				menuHelper.audioPlayerMenu.unlockTrack(4);
			};
			if(!menuHelper.audioPlayerMenu.unlockedArray[5]){
				menuHelper.audioPlayerMenu.unlockTrack(5);
			};
			if(!menuHelper.audioPlayerMenu.unlockedArray[6]){
				menuHelper.audioPlayerMenu.unlockTrack(6);
			};
			if(!menuHelper.audioPlayerMenu.unlockedArray[7]){
				menuHelper.audioPlayerMenu.unlockTrack(7);
			};
			if(!menuHelper.audioPlayerMenu.unlockedArray[8]){
				menuHelper.audioPlayerMenu.unlockTrack(8);
			};
			if(!menuHelper.audioPlayerMenu.unlockedArray[9]){
				menuHelper.audioPlayerMenu.unlockTrack(9);
			};
		};
	};

	this.collect = function(){
		if(global.playerControls.position.distanceTo(scene.getObjectByName('song2').position) <= 10 && !menuHelper.audioPlayerMenu.unlockedArray[1]){
			menuHelper.audioPlayerMenu.unlockTrack(1);
			menuHelper.trackUnlockedMenu.show();
		}
		if(global.playerControls.position.distanceTo(scene.getObjectByName('song3').position) <= 10 && !menuHelper.audioPlayerMenu.unlockedArray[2]){
			menuHelper.audioPlayerMenu.unlockTrack(2);
			menuHelper.trackUnlockedMenu.show();
		}
		if(global.playerControls.position.distanceTo(scene.getObjectByName('song5').position) <= 10 && !menuHelper.audioPlayerMenu.unlockedArray[4]){
			menuHelper.audioPlayerMenu.unlockTrack(4);
			menuHelper.trackUnlockedMenu.show();
		}
		if(global.playerControls.position.distanceTo(scene.getObjectByName('song6').position) <= 10 && !menuHelper.audioPlayerMenu.unlockedArray[5]){
			menuHelper.audioPlayerMenu.unlockTrack(5);
			menuHelper.trackUnlockedMenu.show();
		}
		if(global.playerControls.position.distanceTo(scene.getObjectByName('song8').position) <= 10 && !menuHelper.audioPlayerMenu.unlockedArray[7]){
			menuHelper.audioPlayerMenu.unlockTrack(7);
			menuHelper.trackUnlockedMenu.show();
		}
		if(global.playerControls.position.distanceTo(scene.getObjectByName('song9').position) <= 10 && !menuHelper.audioPlayerMenu.unlockedArray[8]){
			menuHelper.audioPlayerMenu.unlockTrack(8);
			menuHelper.trackUnlockedMenu.show();
		}
		if(global.playerControls.position.distanceTo(scene.getObjectByName('song10').position) <= 10 && !menuHelper.audioPlayerMenu.unlockedArray[9]){
			menuHelper.audioPlayerMenu.unlockTrack(9);
			menuHelper.trackUnlockedMenu.show();
		};
	};

	this.finishedTrack = function(trackIndex){
		tracksFinished[trackIndex] = true;
		if(tracksFinished[0]&&tracksFinished[1]&&tracksFinished[2]&&tracksFinished[3]&&tracksFinished[4]&&tracksFinished[5]&&tracksFinished[6]&&tracksFinished[7]&&tracksFinished[8]&&tracksFinished[9]&&!global.completedGame){
			menuHelper.completedMenu.show();
			global.playerControls.enabled = false;
			global.completedGame = true;
		};
	};

	this.dispose = function(){
		window.removeEventListener( 'keyup', onKeyUp, false);
	};

	scope.addListener();
};