/*
Kevin R. Justice
041817
InteractionHelper.js

helper for interacting with in game content
*/

InteractionHelper = function(){
	var scope = this;

	var menuShown = false;
	var interactionLocations = [ new THREE.Vector3(58.5,10,26), new THREE.Vector3(28,10,23), new THREE.Vector3(54.5,10,-27.5)];
	var links = [ 'https://www.youtube.com/watch?v=Hbha6Ut1CAs', 'https://www.youtube.com/playlist?list=PL4KdOYpAdFTmZcwUH1wlG_hKFOJgm9Xab', 'https://www.youtube.com/watch?v=lb8E_6L59zw'];

	var onKeyUp = function(e){
		if(e.keyCode == 73){
			scope.interact();
		};
	};

	this.addListener = function(){
		window.addEventListener( 'keyup', onKeyUp, false);
	};

	this.update = function(){
		var inRange = false;
		for(var i=0; i<interactionLocations.length; i++){
			if(global.playerControls.position.distanceTo(interactionLocations[i])<=10){
				inRange = true;
			};
		};
		if(inRange && !menuShown){
			menuHelper.interactionMenu.show();
			menuShown = true;
		}
		else if(!inRange && menuShown){
			menuHelper.interactionMenu.hide();
			menuShown = false;
		};
	};

	this.interact = function(){
		if(menuShown){
			var distances = [global.playerControls.position.distanceTo(interactionLocations[0]), global.playerControls.position.distanceTo(interactionLocations[1]), global.playerControls.position.distanceTo(interactionLocations[2])];
			var min = Math.min.apply(Math, distances);
			var currentLink = links[distances.indexOf(min)];
			window.open(currentLink,'_blank');
		};
	};

	this.dispose = function(){
		window.removeEventListener( 'keyup', onKeyUp, false);
	};

	this.addListener();
};