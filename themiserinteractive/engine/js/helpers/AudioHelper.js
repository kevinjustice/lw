/*
Kevin R. Justice
040617
AudioHelper.js

helper for playing audio
*/

AudioHelper = function(){
	var scope = this;

	var track = function(){
		var subScope = this;

		this.volume = 1;
		this.fadingIn = false;
		this.playing = false;

		var audioLocal;
		var lastUpdate;
		
		this.play = function(audioBuffer){
			if(subScope.playing){
				subScope.stop();
			};
			var audioListener = new THREE.AudioListener();
			audioLocal = new THREE.Audio(audioListener);
			audioLocal.setBuffer(audioBuffer);
			audioLocal.setVolume(subScope.volume);
			audioLocal.play();
			subScope.playing = true;
		};

		this.stop = function(){
			if(subScope.playing){
				audioLocal.stop();
			};
			subScope.playing = false;
		};

		this.loopEnabled = function(loopTrue){
			audioLocal.setLoop(loopTrue);
		};

		this.setVolume = function(volume){
			if(subScope.playing){
				audioLocal.setVolume(volume);
			};
			subScope.volume = volume;
		};

		this.fadeIn = function(){
			subScope.fadingIn = true;
			lastUpdate = performance.now()*.001;
		};

		this.update = function(){
			if(subScope.fadingIn){
				if(subScope.volume>=1){
					subScope.fadingIn = false;
					subScope.volume = 1;
				}
				else{
					var now = performance.now()*.001;
					subScope.setVolume(subScope.volume+(now-lastUpdate)/3);
					lastUpdate = now;
				};
			};
		};
	};

	this.sfxTrack = new track();
	this.musicTrack = new track();
};