/*
Kevin R. Justice
040217
CharacterSelectionHelper.js

updates character for character selection screen
*/

CharacterSelectionHelper = function(){
	var scope = this;

	this.currentCharacter = menuHelper.characterSelectionMenu.currentCharacter;
	/*
	0 Matthew
	1 Stevie
	2 Quinn
	3 Ryan
	4 Miser
	*/
	this.update = function(){
		var currentSelected = menuHelper.characterSelectionMenu.currentCharacter;
		if(scope.currentCharacter!=currentSelected){
			scene.remove(characters[scope.currentCharacter]);
			scene.add(characters[currentSelected]);
			scope.currentCharacter = currentSelected;
		};
	};

	var matthew = global.models.characters.matthew_stand.clone(); matthew.scale.set(1.05,1.05,1.05);
	var stevie = global.models.characters.stevie_stand.clone();
	var quinn = global.models.characters.quinn_stand.clone();
	var ryan = global.models.characters.ryan_stand.clone(); ryan.scale.set(.97,.97,.97);
	var miser = global.models.characters.miser_stand.clone(); miser.scale.set(.95,.95,.95);
	var characters = [matthew,stevie,quinn,ryan,miser];

	scene.add(characters[scope.currentCharacter]);
};