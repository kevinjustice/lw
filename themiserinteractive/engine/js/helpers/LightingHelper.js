/*
Kevin R. Justice
040917
LightingHelper.js

helper for unlocking in game content
*/

LightingHelper = function(){
	var scope = this;

	this.outdoor = true;

	this.update = function(){
		if((typeof global.playerControls != 'undefined') && (typeof scene.getObjectByName('hemisphereLight') != 'undefined')){
			var z = global.playerControls.position.z;
			if(!scope.outdoor && (z<=-38.5 || z>=43.5)){
				scene.getObjectByName('hemisphereLight').intensity = 1;
				scope.outdoor = true;
			}
			else if(scope.outdoor && (z>-38.5 && z<43.5)){
				scene.getObjectByName('hemisphereLight').intensity = 0;
				scope.outdoor = false;
			};
		};
	};
};