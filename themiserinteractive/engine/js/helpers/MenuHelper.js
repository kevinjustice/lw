/*
Kevin R. Justice
033017
MenuHelper.js

draws and undraws html menus
*/

MenuHelper = function(){
	var scope = this;

	var menu1 = function(){
	/*
	loadingMenu
	*/
		var subScope = this;

		this.modelsLoaded = 0;
		this.shown = false;
		this.progress = 0;

		this.show = function(){
			if(!subScope.shown){
				var loadingScreenBackground = document.createElement('div'); loadingScreenBackground.id = 'loadingScreenBackground';
				document.body.appendChild(loadingScreenBackground);

				var loadingScreenHeader = document.createElement('div'); loadingScreenHeader.id = 'loadingScreenHeader';
				loadingScreenHeader.innerHTML = "The Miser Interactive is Loading<br/><div id='loadingScreenSubHeader'>This May Take a While...</div>";
				document.body.appendChild(loadingScreenHeader);

				var loadingBarContainer = document.createElement('div'); loadingBarContainer.id = 'loadingBarContainer';
				var loadingBarBorder = document.createElement('div'); loadingBarBorder.id = 'loadingBarBorder';
				var loadingBar = document.createElement('div'); loadingBar.id = 'loadingBar';
				loadingBarBorder.appendChild(loadingBar); loadingBarContainer.appendChild(loadingBarBorder);
				document.body.appendChild(loadingBarContainer);

				document.getElementById('loadingBar').style.width = (subScope.progress*780).toString()+'px';

				audio.musicTrack.play(global.audio.music.grooveSynth);
				audio.musicTrack.loopEnabled(true);

				subScope.shown = true;
			};
		};

		this.hide = function(){
			if(subScope.shown){
				document.getElementById('loadingScreenBackground').outerHTML="";
				document.getElementById('loadingScreenHeader').outerHTML="";
				document.getElementById('loadingBarContainer').outerHTML="";

				audio.musicTrack.stop();

				subScope.shown = false;
			};
		};

		this.updateProgress = function(numModels){
			subScope.modelsLoaded += 1;
			subScope.progress += 1/numModels;
			if(subScope.shown){
				if(subScope.progress<=1.0001){
					document.getElementById('loadingBar').style.width = (subScope.progress*780).toString()+'px';
				}
				else{
					subScope.setProgress(1);
					console.warn('Loading progress has exceeded 100%!');
				};
			}
			else{
				if(subScope.progress>1.0001){
					subScope.setProgress(1);
					console.warn('Loading progress has exceeded 100%!');
				};
			};
		};

		this.resetProgress = function(){
			subScope.progress = 0;
			if(subScope.shown){
				document.getElementById('loadingBar').style.width = '0px';
			};
		};

		this.setProgress = function(progressNew){
			if(progressNew <= 1 && progressNew > 0){
				subScope.progress = progressNew;
				if(subScope.shown){
					document.getElementById('loadingBar').style.width = (subScope.progress*780).toString()+'px';
				};
			}
			else{
				console.warn('You cannot set a loading progress of more than 100% or less than or equal to 0%!');
			};
		};
	};

	var menu2 = function(){
		/*
		cover
		*/
		var subScope = this;

		this.shown = false;

		this.show = function(){
			if(!subScope.shown){
				var background = document.createElement('div'); background.id = 'coverBackground';
				var text = document.createElement('div'); text.id = 'coverText'; text.innerHTML = "The Miser Interactive";
				background.appendChild(text);
				document.body.appendChild(background);

				subScope.shown = true;
			};
		};

		this.hide = function(){
			if(subScope.shown){
				document.getElementById('coverBackground').outerHTML="";

				subScope.shown = false;
			};
		};
	};

	var menu3 = function(){
		/*
		character selection
		*/
		var subScope = this;

		this.miserEnabled = false;
		this.currentCharacter = 0;
		this.shown = false;

		var matthewBio = "Thrashability: 10/10<br/><br />&#34<i>The drumline is oodles of fun. It's a great and local pure rock total sass extravaganza. In our primal ritual drum beatings the stars align, and we connect with the origins of the universe.&#34</i>";
		var stevieBio = "Shredability: 10/10<br/><br />&#34<i>So, I've been told that I'm a sick, sick man; don't question my dope rhymes man, just because you can. Er.&#34</i>";
		var quinnBio = "Groovability: 10/10<br/><br />&#34<i>I honestly don't care what it is as long as it's funky, man.&#34</i>";
		var ryanBio = "Jamability: 10/10<br/><br />&#34<i>Well, I was told I would be contacted about goin to Richard's old house to hang out, and have yet to be contacted about such things. Also, if y'all start to hang after 10 I'm not goin.&#34</i>";
		var miserBio = "Miserability: 10/10<br/><br />&#34<i>The last time I tried one of those get rich quick schemes, I ended up buying a lifetime supply of girl scout cookies with no prospects of selling them all.&#34</i>";

		var bios = [matthewBio, stevieBio, quinnBio, ryanBio, miserBio];
		var names = ['Matthew', 'Stevie', 'Quinn', 'Ryan', 'The Miser'];
		var elements = ['matthewCharacterMenuBackground', 'stevieCharacterMenuBackground', 'quinnCharacterMenuBackground', 'ryanCharacterMenuBackground', 'miserCharacterMenuBackground'];

		this.show = function(){
			if(!subScope.shown){
				var left = document.createElement('div'); left.id = 'leftCharacterMenuWrapper';
				left.innerHTML = '<div id="leftCharacterMenuHeader"><b>Select a Character!</b></div><div id="matthewCharacterMenuBackground" class="unselectedCharacterMenuBackground" onclick="menuHelper.characterSelectionMenu.setCharacter(0)"><div id="matthewCharacterMenuHeadshot"></div><div id="matthewCharacterMenuName"><b><i>Matthew</i></b></div></div><div id="stevieCharacterMenuBackground" class="unselectedCharacterMenuBackground" onclick="menuHelper.characterSelectionMenu.setCharacter(1)"><div id="stevieCharacterMenuHeadshot"></div><div id="stevieCharacterMenuName"><b><i>Stevie</i></b></div></div><div id="quinnCharacterMenuBackground" class="unselectedCharacterMenuBackground" onclick="menuHelper.characterSelectionMenu.setCharacter(2)"><div id="quinnCharacterMenuHeadshot"></div><div id="quinnCharacterMenuName"><b><i>Quinn</i></b></div></div><div id="ryanCharacterMenuBackground" class="unselectedCharacterMenuBackground" onclick="menuHelper.characterSelectionMenu.setCharacter(3)"><div id="ryanCharacterMenuHeadshot"></div><div id="ryanCharacterMenuName"><b><i>Ryan</i></b></div></div><div id="unknownCharacterMenuBackground" class="unselectedCharacterMenuBackground"><div id="unknownCharacterMenuHeadshot"></div><div id="unknownCharacterMenuName"><b><i>Unknown</i></b></div></div>';			
				document.body.appendChild(left);

				var right = document.createElement('div'); right.id = 'rightCharacterMenuWrapper';
				right.innerHTML = '<div id="rightCharacterMenuHeader"></div><div id="characterMenuBio"></div><div id="characterMenuLaunchButton" onclick="menuHelper.characterSelectionMenu.selected()"><b>Play!</b></div>';
				document.body.appendChild(right);

				var bottom = document.createElement('div'); bottom.id = 'characterMenuSelectionDirections';
				bottom.innerHTML = 'Click and Drag to Rotate Character';
				document.body.appendChild(bottom);

				if(subScope.miserEnabled){
					document.getElementById('unknownCharacterMenuBackground').outerHTML = '<div id="miserCharacterMenuBackground" class="unselectedCharacterMenuBackground" onclick="menuHelper.characterSelectionMenu.setCharacter(4)"><div id="miserCharacterMenuHeadshot"></div><div id="miserCharacterMenuName"><b><i>The Miser</i></b></div></div>'; 
				};

				subScope.shown = true;
				subScope.setCharacter(subScope.currentCharacter);
			};
		};

		this.hide = function(){
			if(subScope.shown){
				document.getElementById('leftCharacterMenuWrapper').outerHTML = "";
				document.getElementById('rightCharacterMenuWrapper').outerHTML = "";
				document.getElementById('characterMenuSelectionDirections').outerHTML = "";

				subScope.shown = false;
			};
		};

		this.setCharacter = function(index){
			if(subScope.shown){
				document.getElementById('rightCharacterMenuHeader').innerHTML = names[index];
				document.getElementById('characterMenuBio').innerHTML = bios[index];
				document.getElementById(elements[subScope.currentCharacter]).className = 'unselectedCharacterMenuBackground';
				document.getElementById(elements[index]).className = 'selectedCharacterMenuBackground';
			};
			subScope.currentCharacter = index;
		};

		this.selected = function(){
			console.info('Character Selected.');
			launch(miserMainSceneInit);
		};

		this.activateMiser = function(){
			if(!subScope.miserEnabled){
				subScope.miserEnabled = true;
				if(subScope.shown){
					document.getElementById('unknownCharacterMenuBackground').outerHTML = '<div id="miserCharacterMenuBackground" class="unselectedCharacterMenuBackground" onclick="menuHelper.characterSelectionMenu.setCharacter(4)"><div id="miserCharacterMenuHeadshot"></div><div id="miserCharacterMenuName"><b><i>The Miser</i></b></div></div>';
					
					audio.sfxTrack.play(global.audio.sfx.miserUnlock);
				};
			};
		};
	};

	var menu4 = function(){
		/*
		about
		*/
		var subScope = this;

		this.shown = false;

		this.show = function(){
			if(!subScope.shown){
				var background = document.createElement('div'); background.id = 'aboutScreenBackground';
				background.innerHTML = "<div id='aboutScreenHeader'>The Miser Interactive</div><div id='aboutScreenAbout'>is a virtual, three dimensional, interactive album listening experience. Due to flashing lights and sounds in this program, individuals with epilepsy or those who are prone to seizures should not continue.</div><div id='aboutScreenContinueButton' onclick='menuHelper.aboutScreen.continue()'>Continue</div>";			
				document.body.appendChild(background);

				subScope.shown = true;
			};
		};

		this.hide = function(){
			if(subScope.shown){
				document.getElementById('aboutScreenBackground').outerHTML = "";

				subScope.shown = false;
			};
		};

		this.continue = function(){
			launch(loadFilesInit);
		};
	};

	var menu5 = function(){
		/*
		incompatible
		*/
		var subScope = this;

		this.shown = false;

		this.show = function(){
			if(!subScope.shown){
				var background = document.createElement('div'); background.id = 'incompatibleScreenBackground';
				background.innerHTML = "<div id='incompatibleScreenHeader'>It appears that your browser is incompatible with<div id='incompatibleScreenSubHeader'>The Miser Interactive</div></div><div id='incompatibleScreenInfo'>Fear not! For information on how to fix this, <a href='/themiserinteractive/troubleshooting.html' target='_blank'>click here</a>.</div>";			
				document.body.appendChild(background);

				subScope.shown = true;
			};
		};

		this.hide = function(){
			if(subScope.shown){
				document.getElementById('incompatibleScreenBackground').outerHTML = "";
				
				subScope.shown = false;
			};
		};
	};

	var menu6 = function(){
		/*
		audio player
		*/
		var subScope = this;

		this.shown = false;

		this.currentTrack = {trackIndex:0, finishTime:Infinity};
		this.lastTrack = 6;

		this.unlockedArray = [false, false, false, false, false, false, false, false, false, false];
		var trackElementIds = ['audioBox0', 'audioBox1', 'audioBox2', 'audioBox3', 'audioBox4','audioBox5','audioBox6', 'audioBox7', 'audioBox8', 'audioBox9'];
		var trackNames = ['Groovefuzz', 'Drano', 'The Miser', 'I Wanna Disappear', 'Lovely Trip', 'Acid Face', 'Light Race Space Death', 'Eviscerator', 'Minds Eye', 'Rut'];
		var playTimes = [181, 129, 123, 254, 172, 146, 214, 174, 173, 388];

		this.show = function(){
			if(!subScope.shown){
				var wrapper = document.createElement('div'); wrapper.id = 'audioPlayerWrapper';
				wrapper.innerHTML = '<div id="audioBox0" class="unselectableAudioBox"></div><div id="audioBox1" class="unselectableAudioBox"></div><div id="audioBox2" class="unselectableAudioBox"></div><div id="audioBox3" class="unselectableAudioBox"></div><div id="audioBox4" class="unselectableAudioBox"></div><div id="audioBox5" class="unselectableAudioBox"></div><div id="audioBox6" class="unselectableAudioBox"></div><div id="audioBox7" class="unselectableAudioBox"></div><div id="audioBox8" class="unselectableAudioBox"></div><div id="audioBox9" class="unselectableAudioBox"></div>';
				document.body.appendChild(wrapper);

				subScope.shown = true;
				
				for(var i=0; i<10; i++){
					if(subScope.unlockedArray[i]){
						subScope.unlockTrack(i);
					};
				};
			};
		};

		this.hide = function(){
			if(subScope.shown){
				subScope.lastTrack = subScope.currentTrack.trackIndex;
				subScope.currentTrack = {trackIndex:0, finishTime:Infinity};
				audio.musicTrack.stop();
				document.getElementById('audioPlayerWrapper').outerHTML = "";
				
				subScope.shown = false;
			};
		};

		this.unlockTrack = function(trackIndex){
			if(subScope.shown){
				document.getElementById(trackElementIds[trackIndex]).innerHTML = trackNames[trackIndex];
				document.getElementById(trackElementIds[trackIndex]).className = 'unselectedAudioBox';
				document.getElementById(trackElementIds[trackIndex]).onclick = function(){menuHelper.audioPlayerMenu.selectTrack(trackIndex)};
			};
			subScope.unlockedArray[trackIndex] = true;
		};

		this.selectTrack = function(trackIndex){
			if(subScope.shown){
				document.getElementById(trackElementIds[subScope.currentTrack.trackIndex]).className = 'unselectedAudioBox';
				document.getElementById(trackElementIds[trackIndex]).className = 'selectedAudioBox';
				subScope.currentTrack.trackIndex = trackIndex;
				subScope.currentTrack.finishTime = performance.now()*.001+playTimes[trackIndex];

				switch(trackIndex){
					case 0:
						audio.musicTrack.play(global.audio.music.grooveFuzz);
						break;
					case 1:
						audio.musicTrack.play(global.audio.music.drano);
						break;
					case 2:
						audio.musicTrack.play(global.audio.music.theMiser);
						break;
					case 3:
						audio.musicTrack.play(global.audio.music.iWannaDisappear);
						break
					case 4:
						audio.musicTrack.play(global.audio.music.lovelyTrip);
						break;
					case 5:
						audio.musicTrack.play(global.audio.music.acidFace);
						break;
					case 6:
						audio.musicTrack.play(global.audio.music.lightRaceSpaceDeath);
						break;
					case 7:
						audio.musicTrack.play(global.audio.music.eviscerator);
						break;
					case 8:
						audio.musicTrack.play(global.audio.music.mindsEye);
						break;
					case 9:
						audio.musicTrack.play(global.audio.music.rut);
						break;
				};
			};
		};

		this.update = function(){
			var now = performance.now()*.001;
			if(now > subScope.currentTrack.finishTime){
				global.unlockHelper.finishedTrack(subScope.currentTrack.trackIndex);
				for(var i=1; i<11; i++){
					if(subScope.unlockedArray[(subScope.currentTrack.trackIndex+i)%10]){
						subScope.selectTrack((subScope.currentTrack.trackIndex+i)%10);
						break;
					};
				};
			};
		};
	};

	var menu7 = function(){
		/*
		tutorial menu
		*/
		var subScope = this;

		this.shown = false;

		this.show = function(){
			if(!subScope.shown){
				var background = document.createElement('div'); background.id = 'tutorialMenuBackground';
				document.body.appendChild(background);

				var content = document.createElement('div'); content.id ='tutorialMenuWrapper';
				content.innerHTML = '<div id="tutorialMenuDialogue">Hey! Welcome to The Miser Interactive, and thanks for playing! Before I let you go, let me explain the controls...<br/><br/>Press the <b>[W]</b> key to move forward.<br/>Press the <b>[S]</b> key to move backward.<br/>Press the <b>[A]</b> key to move left.<br/>Press the <b>[D]</b> key to move right.<br/><br/>Look around with the <b>Arrow Keys</b>, and try to find and collect all of the songs! When you find a song collect it with the <b>[C]</b> key.<br/><br/>Listen to it using the player at the <b>Bottom</b> of the screen. Just <b>Click</b> on the song you want to play.<br /><br />To mute the audio player or switch characters, use the <b>Buttons</b> located in the <b>Top Right</b> corner of the screen.<br/><br/>You can interact with certain objects by pressing the <b>[I]</b> key near them, when the interaction menu pops up.<br /><br />I already added three songs for you, so sit back and relax or go find those songs!<div id="tutorialMenuCloseButton" onclick="menuHelper.tutorialMenu.hide()"><b>OK!</b></div></div>';
				document.body.appendChild(content);

				subScope.shown = true;
			};
		};

		this.hide = function(){
			if(subScope.shown){
				document.getElementById('tutorialMenuBackground').outerHTML = "";
				document.getElementById('tutorialMenuWrapper').outerHTML = "";

				audio.sfxTrack.stop();
				scope.audioPlayerMenu.selectTrack(scope.audioPlayerMenu.lastTrack);
				global.playerControls.enabled = true;

				subScope.shown = false;
			};
		};

		this.playNarration = function(){
			if(subScope.shown){
				switch(scope.characterSelectionMenu.currentCharacter){
					case 0:
						audio.sfxTrack.play(global.audio.sfx.matthewTutorialNarration);
						break;
					case 1:
						audio.sfxTrack.play(global.audio.sfx.stevieTutorialNarration);
						break;
					case 2:
						audio.sfxTrack.play(global.audio.sfx.quinnTutorialNarration);
						break;
					case 3:
						audio.sfxTrack.play(global.audio.sfx.ryanTutorialNarration);
						break;
					case 4:
						audio.sfxTrack.play(global.audio.sfx.miserTutorialNarration);
						break;
				};
			};
		};
	};

	var menu8 = function(){
		/*
		track unlocked
		*/
		var subScope = this;

		this.shown = false;

		this.show = function(){
			if(!subScope.shown){
				var text = document.createElement('div'); text.id = 'trackUnlockedMenu';
				text.innerHTML = 'Track Unlocked!';
				document.body.appendChild(text);

				audio.sfxTrack.play(global.audio.sfx.trackUnlock);
				setTimeout(function(){subScope.hide();}, 3000);

				subScope.shown = true;
			};
		};

		this.hide = function(){
			if(subScope.shown){
				document.getElementById('trackUnlockedMenu').outerHTML="";

				subScope.shown = false;
			};
		};
	};

	var menu9 = function(){
		/*
		switch character
		*/
		var subScope = this;

		this.shown = false;

		this.show = function(){
			if(!subScope.shown){
				var button = document.createElement('div'); button.id = 'switchCharacterMenu'; button.onclick=function(){launch(selectUserInit);};
				button.innerHTML = '<b>Switch Character</b>';
				document.body.appendChild(button);

				subScope.shown = true;
			};
		};

		this.hide = function(){
			if(subScope.shown){
				document.getElementById('switchCharacterMenu').outerHTML="";

				subScope.shown = false;
			};
		};
	};

	var menu10 = function(){
		/*
		mute
		*/
		var subScope = this;

		this.shown = false;
		this.muted = false;

		this.show = function(){
			if(!subScope.shown){
				var button = document.createElement('div'); button.id = 'muteMenu'; button.onclick=function(){menuHelper.muteMenu.toggle()};
				button.innerHTML = '<b>Mute</b>';
				document.body.appendChild(button);

				audio.musicTrack.setVolume(1);

				subScope.shown = true;
			};
		};

		this.hide = function(){
			if(subScope.shown){
				document.getElementById('muteMenu').outerHTML="";

				audio.musicTrack.setVolume(1);

				subScope.shown = false;
			};
		};

		this.toggle = function(){
			if(subScope.shown){
				subScope.muted = !subScope.muted;
				if(subScope.muted){
					audio.musicTrack.setVolume(0);
					document.getElementById('muteMenu').innerHTML = '<b>Un-Mute</b>';
				}
				else{
					audio.musicTrack.setVolume(1);
					document.getElementById('muteMenu').innerHTML = '<b>Mute</b>';
				};
			};
		};
	};

	var menu11 = function(){
		/*
		interaction
		*/
		var subScope = this;

		this.shown = false;

		this.show = function(){
			if(!subScope.shown){
				var text = document.createElement('div'); text.id = 'interactionMenu';
				text.innerHTML = 'Press <b>[I]</b> to Interact!';
				document.body.appendChild(text);

				subScope.shown = true;
			};
		};

		this.hide = function(){
			if(subScope.shown){
				document.getElementById('interactionMenu').outerHTML="";

				subScope.shown = false;
			};
		};
	};

var menu12 = function(){
		/*
		completed
		*/
		var subScope = this;

		this.shown = false;

		this.show = function(){
			if(!subScope.shown){
				audio.musicTrack.setVolume(0);
				audio.sfxTrack.play(global.audio.sfx.miserUnlock);

				var background = document.createElement('div'); background.id = 'completedMenuBackground';
				document.body.appendChild(background);

				var content = document.createElement('div'); content.id ='completedMenuWrapper';
				content.innerHTML = '<div id="completedMenuDialogue">Woah, you finished every song on the album and unlocked a secret character!<br /><br />Type <q>miser</q> while on the character selection screen to claim your prize!<div id="completedMenuCloseButton" onclick="menuHelper.completedMenu.hide()"><b>OK!</b></div></div>';
				document.body.appendChild(content);

				subScope.shown = true;
			};
		};

		this.hide = function(){
			if(subScope.shown){
				document.getElementById('completedMenuBackground').outerHTML = "";
				document.getElementById('completedMenuWrapper').outerHTML = "";

				if(!scope.muteMenu.muted){
					audio.musicTrack.fadeIn();
				};
				global.playerControls.enabled = true;

				subScope.shown = false;
			};
		};
	};

	var menu13 = function(){
		/*
		Setting Up...
		*/
		var subScope = this;

		this.shown = false;

		this.show = function(){
			if(!subScope.shown){
				var background = document.createElement('div'); background.id = 'setupBackground';
				document.body.appendChild(background);

				subScope.shown = true;
			};
		};

		this.hide = function(){
			if(subScope.shown){
				document.getElementById('setupBackground').outerHTML="";

				subScope.shown = false;
			};
		};
	};

	this.incompatibleScreen = new menu5();
	this.aboutScreen = new menu4();
	this.loadingMenu = new menu1();
	this.cover = new menu2();
	this.characterSelectionMenu = new menu3();
	this.audioPlayerMenu = new menu6();
	this.tutorialMenu = new menu7();
	this.trackUnlockedMenu = new menu8();
	this.switchCharacterMenu = new menu9();
	this.muteMenu = new menu10();
	this.interactionMenu = new menu11();
	this.completedMenu = new menu12();
	this.setupMenu = new menu13();
};