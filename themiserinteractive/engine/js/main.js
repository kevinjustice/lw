/*
Kevin R. Justice
022717
main.js

main script for engine
*/

var scene, camera, audio, renderer, animateLoop;

var animating = false;

var destroyFunction = function(){console.info('Ready For Initial Scene.')};

var menuHelper = new MenuHelper();
var global = new function(){};
global.models = new function(){};
global.models.structures = new function(){};
global.models.props = new function(){};
global.models.characters = new function(){};
global.audio = new function(){};
global.audio.sfx = new function(){};
global.audio.music = new function(){};
global.fonts = new function(){};
global.video = new function(){};

global.initializationErrors = false;

function launch(init){
	animating = false;
	destroyFunction();
	init();
	setTimeout(function(){ animating = true; animate(); }, 300);
};

try{
	//var stats = new Stats();
	//stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
	//document.body.appendChild(stats.dom);

	scene = new THREE.Scene();
	scene.fog = new THREE.FogExp2(0xffffff, 0);

	var WIDTH = 1024;
	var HEIGHT = 576;
	renderer = new THREE.WebGLRenderer({antialias:true});
	renderer.setSize(WIDTH, HEIGHT);
	renderer.setClearColor(0x0, 1);
	document.body.appendChild(renderer.domElement);

	camera = new THREE.PerspectiveCamera(75, WIDTH / HEIGHT, 0.1, 2000);
	camera.position.set(0,0,0);
	camera.rotation.set(0,0,0);
	scene.add(camera);

	audio = new AudioHelper();

/*
	var testImg = document.createElement('img'); testImg.src = '/themiserinteractive/engine/images/blank.JPG?_='+(new Date().getTime()).toString();
	testImg.setAttribute('crossorigin', 'anonymous');
	var testCanvas = document.createElement( 'canvas' );
	testCanvas.width = 1;
	testCanvas.height = 1;
	testCanvas.getContext('2d').fillRect(0,0,1,1);
	testImg.onload = function(){global.initializationErrors = false;try{testCanvas.getContext('2d').drawImage(testImg, 0, 0);testCanvas.toDataURL();}catch(e){global.initializationErrors = true;};};
*/
}
catch(e){
	global.initializationErrors = true;
};

function animate(){
	if(animating){
		requestAnimationFrame(animate);
		//stats.begin();
		animateLoop();
		//stats.end();
		renderer.render(scene, camera);
	};
};